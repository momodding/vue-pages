/* eslint-disable */
import Vue from 'vue';
import axios from 'axios';
import constant from './constant'
import { BootstrapVue, BootstrapVueIcons } from 'bootstrap-vue';
import VueSweetalert2 from 'vue-sweetalert2';
import IdleVue from "idle-vue";

import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';
import 'sweetalert2/dist/sweetalert2.min.css';
import { library, dom } from '@fortawesome/fontawesome-svg-core';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { fab } from '@fortawesome/free-brands-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';

import App from './App';
import router from './router';
import store from './store/store';

library.add(fas)
library.add(fab)
dom.watch()

Vue.prototype.$axios = axios;
Vue.prototype.$axios.interceptors.request.use(
  config => {
    if (store.getters.isLoggedIn) config.headers['X-Token-Auth'] = localStorage.getItem('token');
    return config;
  }
);
Vue.prototype.$axios.interceptors.response.use(function (response) {
  return Promise.resolve(response.data)
}, function (error) {
  const originalRequest = error.config;
  // console.log(originalRequest)
  if (error.response.status === 401 &&
      originalRequest.url === constant.getNewBaseApiUrl() + '/v1/auth/login') {
    store.dispatch('logout')
      .then(() => {
        if (router.currentRoute.name !== 'login') {
          router.push('/login')
        }
      });
    return Promise.reject(error.response.data.status)
  }
  if (error.response.status === 401 &&
    !originalRequest._retry) {
    originalRequest._retry = true;
    return axios({
      url: constant.getNewBaseApiUrl() + '/v1/auth/token?refreshToken=' + localStorage.getItem('refresh_token'),
      method: 'GET',
    })
    .then(res => {
      if (res.meta.code === 200) {
        const token = res.data.accessToken;
        localStorage.setItem('token', token);
        axios.defaults.headers.common['X-Token-Auth'] = token;
        return axios(originalRequest);
      }
    })
    .catch(err => {
      store.dispatch('logout')
        .then(() => {
          if (router.currentRoute.name !== 'login') {
            router.push('/login')
          }
        });
    })
  }
    return Promise.reject(error ? error : {response: "Something went wrong!"})
});
Vue.prototype.$constants = constant;

Vue.config.productionTip = false;

Vue.use(BootstrapVue);
Vue.use(BootstrapVueIcons);
Vue.use(VueSweetalert2);

Vue.component('font-awesome-icon', FontAwesomeIcon)

/* eslint-disable no-new */
const appVue = new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>',
  created() {
    if (sessionStorage.redirect && constant.getEnvironment() === 'production') {
      const redirect = sessionStorage.redirect
      delete sessionStorage.redirect
      this.$router.push(redirect)
    }
  },
});

if (constant.getEnvironment() != 'development') {
  Vue.use(IdleVue, {
    eventEmitter: appVue,
    store,
    idleTime: 600000, // 3 seconds,
    startAtIdle: false
  });
}
