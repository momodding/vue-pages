/* eslint-disable */
const constant = {
  baseUrl: "https://nest-js-be.herokuapp.com",
  getGitProjectName() {
    return '/' + process.env.CI_PROJECT_NAME + '/';
  },
  getEnvironment() {
    return process.env.NODE_ENV;
  },
  getBaseApiUrl() {
    return process.env.BASE_API_URL;
  },
  getNewBaseApiUrl() {
    return process.env.NEW_BASE_API_URL;
  },
  getBaseBoUrl() {
    return process.env.BASE_BO_URL;
  },
  getAdminRole() {
    return process.env.USER_ROLE_ADMIN;
  },
  getRegulerRole() {
    return process.env.USER_ROLE_REGULER;
  },
  getUserProfile() {
    const user = localStorage.getItem('user')
    return user ? JSON.parse(user) : null;
  }
}

export default constant;
