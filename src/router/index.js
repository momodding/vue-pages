/* eslint-disable */
import Vue from 'vue';
import Router from 'vue-router';

import store from '../store/store';
import constants from "../constant";

import HelloWorld from '@/components/HelloWorld';
import NotFound from '@/components/NotFoundPage';
import LoginPage from '@/pages/auth/Login';
import RegisterPage from '@/pages/auth/Register';
import LogoutPage from '@/pages/auth/Logout';
import Backoffice from '@/pages/dashboard/Backoffice';
import TodoBoard from '@/pages/dashboard/board/TodoBoard';
import TodoListBoard from '@/pages/dashboard/board/TodoListBoard';
import LandingpageBoard from '@/pages/landingpage/board/LandingpageBoard';
import HomePage from '@/pages/landingpage/layouts/Homepage';

Vue.use(Router);

const router = new Router({
  // base: '/vue-vuex-nest',
  mode: 'history',
  routes: [
    {
      path: '/',
      component: LandingpageBoard,
      children: [
        {
          path: '',
          name: 'Landingpage',
          component: HomePage,
          meta: {
            title: 'Home Page - Portofolio',
            metaTags: [
              {
                name: 'description',
                content: 'Utsman fajar, software enginerr. This is my portofolio page.'
              },
              {
                name: 'keywords',
                content: 'Utsman fajar, utsman, fajar, software enginerr, backend engineer, laravel, machine learning engineer, spring boot'
              },
              {
                property: 'og:description',
                content: 'Utsman fajar, software enginerr. This is my portofolio page.'
              }
            ]
          }
        },
      ]
    },
    {
      path: '/backoffice',
      component: Backoffice,
      children: [
        {
          path: '',
          name: 'Backoffice',
          component: HelloWorld,
          meta: {
            title: 'Backoffice - Home',
            requiresAuth: true,
            userRole: [constants.getAdminRole()]
          },
        },
        {
          path: 'login',
          name: 'loginBo',
          component: LoginPage,
          meta: {
            requiresAuth: true,
            userRole: [constants.getAdminRole()]
          },
        },
        {
          path: 'register',
          name: 'registerBo',
          component: RegisterPage,
          meta: {
            guest: false,
            userRole: [constants.getAdminRole()]
          },
        },
        {
          path: 'todo',
          name: 'todoboardBo',
          component: TodoBoard,
          meta: {
            title: 'Backoffice - Todo',
            requiresAuth: true,
            userRole: [constants.getAdminRole()]
          },
        },
        {
          path: 'todo/list',
          name: 'todolistboardBo',
          component: TodoListBoard,
          meta: {
            title: 'Backoffice - Todo List',
            requiresAuth: true,
            userRole: [constants.getAdminRole()]
          },
        },
      ]
    },
    {
      path: '/login',
      name: 'login',
      component: LoginPage,
      meta: {
        title: 'Backoffice - Login',
        guest: true,
      },
    },
    {
      path: '/logout',
      name: 'logout',
      component: LogoutPage,
      meta: {
        title: 'Backoffice - Logout',
        requiresAuth: true,
      },
    },
    {
      path: '/register',
      name: 'register',
      component: RegisterPage,
      meta: {
        title: 'Backoffice - Register',
        guest: true,
      },
    },
    {
      path: '*',
      name: 'notfound',
      component: NotFound,
      meta: {
        title: 'Page Not Found :(',
        guest: true,
      },
    },
    // {
    //   path: '/dashboard',
    //   name: 'todoboard',
    //   component: TodoBoard,
    //   meta: {
    //     requiresAuth: true,
    //     userRole: [constants.getRegulerRole(), constants.getAdminRole()]
    //   },
    // },
    // {
    //   path: '/dashboard/list',
    //   name: 'todolistboard',
    //   component: TodoListBoard,
    //   meta: {
    //     requiresAuth: true,
    //     userRole: [constants.getRegulerRole(), constants.getAdminRole()]
    //   },
    // },
  ],
});

router.beforeEach((to, from, next) => {
  const nearestWithTitle = to.matched.slice().reverse().find(r => r.meta && r.meta.title);

  // Find the nearest route element with meta tags.
  const nearestWithMeta = to.matched.slice().reverse().find(r => r.meta && r.meta.metaTags);
  const previousNearestWithMeta = from.matched.slice().reverse().find(r => r.meta && r.meta.metaTags);

  // If a route with a title was found, set the document (page) title to that value.
  if (nearestWithTitle) document.title = nearestWithTitle.meta.title;

  // Remove any stale meta tags from the document using the key attribute we set below.
  Array.from(document.querySelectorAll('[data-vue-router-controlled]')).map(el => el.parentNode.removeChild(el));

  store.dispatch('pageRequest', 'loading');
  setTimeout(() => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
      if (!store.getters.isLoggedIn) {
        // Skip rendering meta tags if there are none.
        if (nearestWithMeta) {
          nearestWithMeta.meta.metaTags.map(tagDef => {
            const tag = document.createElement('meta');

            Object.keys(tagDef).forEach(key => {
              tag.setAttribute(key, tagDef[key]);
            });

            // We use this to track which meta tags we create, so we don't interfere with other ones.
            tag.setAttribute('data-vue-router-controlled', '');

            return tag;
          })
            // Add the meta tags to the document head.
          .forEach(tag => document.head.appendChild(tag));
        }
        next({
          path: '/login',
          params: { nextUrl: to.fullPath },
        });
      } else {
        const user = JSON.parse(localStorage.getItem('user'));
        if (nearestWithMeta) {
          nearestWithMeta.meta.metaTags.map(tagDef => {
            const tag = document.createElement('meta');

            Object.keys(tagDef).forEach(key => {
              tag.setAttribute(key, tagDef[key]);
            });

            // We use this to track which meta tags we create, so we don't interfere with other ones.
            tag.setAttribute('data-vue-router-controlled', '');

            return tag;
          })
            // Add the meta tags to the document head.
            .forEach(tag => document.head.appendChild(tag));
        }
        if (to.matched.some(record => record.meta.userRole)) {
          if (to.meta.userRole.includes(user.role.toString())) {
            next();
          } else {
            next({ name: 'Landingpage' });
          }
        } else {
          next();
        }
      }
    } else if (to.matched.some(record => record.meta.guest)) {
      if (nearestWithMeta) {
        nearestWithMeta.meta.metaTags.map(tagDef => {
          const tag = document.createElement('meta');

          Object.keys(tagDef).forEach(key => {
            tag.setAttribute(key, tagDef[key]);
          });

          // We use this to track which meta tags we create, so we don't interfere with other ones.
          tag.setAttribute('data-vue-router-controlled', '');

          return tag;
        })
          // Add the meta tags to the document head.
          .forEach(tag => document.head.appendChild(tag));
      }
      if (!store.getters.isLoggedIn) {
        next();
      } else {
        next({ name: 'Landingpage' });
      }
    } else {
      if (nearestWithMeta) {
        nearestWithMeta.meta.metaTags.map(tagDef => {
          const tag = document.createElement('meta');

          Object.keys(tagDef).forEach(key => {
            tag.setAttribute(key, tagDef[key]);
          });

          // We use this to track which meta tags we create, so we don't interfere with other ones.
          tag.setAttribute('data-vue-router-controlled', '');

          return tag;
        })
          // Add the meta tags to the document head.
          .forEach(tag => document.head.appendChild(tag));
      }
      next();
    }
  }, 1000);
});

router.afterEach(() => {
  store.dispatch('pageRequest', '');
})

export default router;
