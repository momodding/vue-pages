/* eslint-disable */
import Vue from 'vue'
import Vuex from 'vuex'
// import axios from 'axios'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    status: '',
    respMessage: null,
    token: localStorage.getItem('token') || '',
    user: {},
    todo: {},
    todos: [{}],
    idleVue: '',
  },
  mutations: {
    auth_request: (state, status) => {
      state.status = status
    },
    auth_success: (state, {user, token}) => {
      state.status = 'success'
      state.user = user
      state.token = token
      state.respMessage = ''
    },
    request_error: (state, {message}) => {
      state.status = 'error'
      state.respMessage = message
    },
    logout: (state) => {
      state.status = ''
      state.token = ''
      state.respMessage = ''
    },
    todo_list: (state, todos) => {
      state.status = 'success'
      state.todos = todos
    },
    todo_post: (state, todo) => {
      state.status = 'success'
      state.todo = todo
      state.todos.push(todo)
    },
    todo_update: (state, todo) => {
      state.status = 'success'
      state.todo = todo
      state.todos.filter((el, index) => {
        if (el.id == todo.id) {
          // console.log('element: ', el.id, 'todo: ', todo.id)
          // state.todos[index] = todo
          Vue.set(state.todos, index, todo);
        }
      })
    },
    todo_delete: (state, id) => {
      state.status = 'success'
      state.todos = [...state.todos.filter(el => el.id !== id)]
    },
    todo_edit: (state, todo) => {
      state.todo = todo
    }
  },
  actions: {
    pageRequest({ commit }, status) {
      return new Promise((resolve, reject) => {
        commit('auth_request', status);
        resolve();
      })
    },
    login({ commit }, user) {
      return new Promise((resolve, reject) => {
        commit('auth_request', 'loading');
        this._vm.$axios({
          url: this._vm.$constants.getNewBaseApiUrl() + '/v1' + '/auth/login',
          data: user,
          method: 'POST' })
          .then((resp) => {
            const token = resp.data.accessToken;
            const refreshToken = resp.data.refreshToken;
            const user = JSON.stringify(resp.data);
            localStorage.setItem('token', token);
            localStorage.setItem('refresh_token', refreshToken);
            localStorage.setItem('user', user);

            commit('auth_success', {token:token, user:JSON.parse(user)});
            resolve(resp);
          })
          .catch((err) => {
            commit('request_error', {message: err.message});
            localStorage.removeItem('user');
            localStorage.removeItem('token');
            reject(err);
          })
      })
    },
    register({ commit }, user) {
      return new Promise((resolve, reject) => {
        commit('auth_request', 'loading');
        this._vm.$axios({ url: this._vm.$constants.getNewBaseApiUrl() + '/v1' + '/auth/register', data: user, method: 'POST' })
          .then(resp => {
            const token = resp.data.accessToken;
            const refreshToken = resp.data.refreshToken;
            const user = JSON.stringify(resp.data);
            localStorage.setItem('token', token);
            localStorage.setItem('refresh_token', refreshToken);
            localStorage.setItem('user', user);
            // this._vm.$axios.defaults.headers.common['Authorization'] = 'Bearer '.token;
            commit('auth_success', { token: token, user: JSON.parse(user) })
            resolve(resp);
          })
          .catch(err => {
            commit('request_error', {message: err.response});
            localStorage.removeItem('token');
            reject(err);
          })
      })
    },
    logout({ commit }) {
      return new Promise((resolve, reject) => {
        commit('logout')
        localStorage.removeItem('token')
        localStorage.removeItem('user')
        delete this._vm.$axios.defaults.headers.common['Authorization']
        resolve();
      })
    },
    getTask({ commit }) {
      return new Promise((resolve, reject) => {
        this._vm.$axios({
          url: this._vm.$constants.getNewBaseApiUrl() + '/v1' + '/todo',
          method: 'GET',
        })
        .then((resp) => {
          commit('todo_list', JSON.parse(JSON.stringify(resp.data)));
          resolve(resp.data);
        })
        .catch((err) => {
          commit('request_error', {message: err.response});
          reject(err);
        });
      });
    },
    createTask({ commit }, todo) {
      return new Promise((resolve, reject) => {
        this._vm.$axios({
          url: this._vm.$constants.getNewBaseApiUrl() + '/v1' + '/todo',
          data: todo, method: 'POST',
        })
        .then((resp) => {
          const todoResp = JSON.parse(JSON.stringify(resp.data));
          commit('todo_post', todoResp);
          resolve(resp.data);
        }).catch((err) => {
          commit('request_error', {message: err.response});
          reject(err);
        });
      })
    },
    updateTask({ commit }, {todo, id}) {
      return new Promise((resolve, reject) => {
        this._vm.$axios({
          url: this._vm.$constants.getNewBaseApiUrl() + '/v1' + '/todo/' + id,
          data: todo, method: 'PUT',
        })
        .then((resp) => {
          const todoResp = JSON.parse(JSON.stringify(resp.data));
          commit('todo_update', todoResp);
          resolve(resp.data);
        }).catch((err) => {
          commit('request_error', {message: err.response});
          reject(err);
        });
      })
    },
    deleteTask({ commit }, id) {
      return new Promise((resolve, reject) => {
        this._vm.$axios({
          url: this._vm.$constants.getNewBaseApiUrl() + '/v1' + '/todo/'+id,
          method: 'DELETE',
        })
        .then((resp) => {
          commit('todo_delete', id);
          resolve(resp);
        }).catch((err) => {
          commit('request_error', {message: err.response});
          reject(err);
        });
      });
    },
    editTask({ commit }, todo) {
      return new Promise((resolve, reject) => {
        commit('todo_edit', todo);
        resolve();
      })
    }
  },
  getters: {
    isLoggedIn: state => !!state.token,
    requestStatus: state => state.status,
    responseMessage: state => state.respMessage,
    todo: state => state.todo,
    todos: state => state.todos,
    idleVue: state => state.idleVue,
  }
})
