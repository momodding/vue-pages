'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  BASE_API_URL: '"https://nest-js-be.herokuapp.com"',
  NEW_BASE_API_URL: '"https://backend-springapi.herokuapp.com"',
  BASE_BO_URL: '"/backoffice"',
  USER_ROLE_ADMIN: '"2"',
  USER_ROLE_REGULER: '"1"',
  configureWebpack: {
    devtool: 'source-map'
  }
})
