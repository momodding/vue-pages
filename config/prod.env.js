'use strict'
module.exports = {
  NODE_ENV: '"production"',
  BASE_API_URL: '"https://nest-js-be.herokuapp.com"',
  NEW_BASE_API_URL: '"https://backend-springapi.herokuapp.com"',
  BASE_BO_URL: '"/backoffice"',
  USER_ROLE_ADMIN: '"2"',
  USER_ROLE_REGULER: '"1"',
}
